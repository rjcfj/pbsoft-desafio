@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Lista Produtos </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}" title="Crie um produto">
                    Crie um produto
                </a>
            </div>
        </div>
    </div>
    @include('layouts.errors')
    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>Codigo</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Categoria</th>
            <th>Preço</th>
            <th>Qualidades</th>
            <th>Ações</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->id}}</td>
            <td>{{ $product->name}}</td>
            <td>{{ $product->description}}</td>
            <td>{{ $product->category}}</td>
            <td>{{ number_format($product->price,2,",",".") }}</td>
            <td>{{ $product->amount}}</td>
            <td>
                <form action="{{ route('products.destroy', [$product->id]) }}" method="POST">
                    <a href="{{ route('products.show', [$product->id]) }}" title="Mostra" style="margin-right: 15px;">Mostrar</a>
                    <a href="{{ route('products.edit', [$product->id]) }}" title="Editar">Editar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" title="Deletar" class="btn btn-link">
                        Deletar
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>
{!! $products->links() !!}
@endsection
