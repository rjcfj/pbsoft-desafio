# PBSOFT Desafio

Processo de Seleção PBSOFT
Implemente um pequeno sistema para gerenciamento de produtos utilizando o conceito de API e WEB

## Servidor:
PHP 7.4.3 / MySQL 5.7.17

## Instalação do Projeto:

~~~~
git clone https://gitlab.com/rjcfj/pbsoft-desafio.git
composer update
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate
~~~~

Você também deve adicionar suas informações de banco de dados em arquivo .env:
~~~~ 
BD: 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=pbsoft
DB_USERNAME=###
DB_PASSWORD=###
~~~~ 

Depois de criar seu banco de dados e fornecer as credenciais, você precisará executar a partir da linha de comando:
~~~~
php artisan migrate
php artisan db:seed
~~~~

Inicie um servidor de desenvolvimento local com `php artisan serve`

Se você prosseguisse com os dados falsos, um usuário deveria ter sido criado para você com as seguintes credenciais de login:

>**email:** `admin@admin.com`   
>**password:** `admin`

## WEB 
#### Telas: Login e Produtos
http://localhost:8000

## API

#### POST Login 
http://localhost:8000/api/login
~~~~
{"email":"#","password":"#"}
~~~~

#### POST Login Registro
http://localhost:8000/api/register
~~~~
{"name":"#","email":"#","password":"#","c_password":"#"}
~~~~

Certifique-se de que na API de detalhes usaremos os seguintes cabeçalhos, conforme listado abaixo:
~~~~
Accept: application/json
Authorization: Bearer #
~~~~

#### POST Logout
http://localhost:8000/api/logout

#### GET Produtos (Lista)
http://localhost:8000/api/products

#### GET Produtos (Buscar por ID)
http://localhost:8000/api/products/{id}

#### POST Produtos (Cadastro)
http://localhost:8000/api/products
~~~~
{"name":"#","description":"#","category":"#","price":"#","amount":"#"}
~~~~

#### PUT Produtos (Atualizar)
http://localhost:8000/api/products/{id}
~~~~
{"name":"#","description":"#","category":"#","price":"#","amount":"#"}
~~~~

### DELETE Produtos (Apagar)
http://localhost:8000/api/products/{id}
